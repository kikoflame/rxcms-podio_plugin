module RxcmsPodioPlugin

  class Executor
    def self.execute(placeholder, attrs, exts)
      # Check if placeholder/element template is bound with a podio item, if not, return static HTML content.
      if (!placeholder.MetadataAssociation.nil?)
        # Just get one app for one placeholder/element
        if (placeholder.MetadataAssociation.length == 1)
          # Check if the items of podio app are specified; if not, show error
          if (!attrs['configs'].nil?)
            if (attrs['configs']['items'].nil?)
              return "{{[rxcms-podio-plugin]WARNING: The items parameter for a podio app for element was not found}}"
            end
          else
            return "{{[rxcms-podio-plugin]WARNING: The configs parameter for element was not found}}"
          end

          placeholderTemplate = placeholder.value.strip
          podio = (placeholder.MetadataAssociation.first).destId

          if (!podio.nil?)
            podioApp = Metadata.find(podio)

            if (!podioApp.nil?)
              appId = podioApp.value

              contentResult = ''
              listItems = attrs['configs']['items'].split('|')
              listItems.each { |i| i.strip }

              # logger.debug(listItems.inspect)
              # Check if the podio app exists?, if not, raise an awesome error

              # logger.debug(appId);
              if (AbstractApplication.app_exists?(appId) == false)
                return "{{[rxcms-podio-plugin]WARNING: The \"#{podioApp.key.strip}\" app doesn't exist or service account doesn't have access to it}}"
              end

              # Use internal method to get a list of items from that podio app
              contentObjs = get_podio_items(appId, listItems)
              # logger.debug(contentObjs.inspect)

              if (attrs['mode'] == 'multiple')

                return "{{[rxcms-podio-plugin]WARNING: The mode \"multiple\" is currently not supported}}"

              elsif (attrs['mode'] == 'alternate')

                if (attrs['configs']['tple'].nil? && attrs['configs']['tplo'].nil?)
                  return "[rxcms-podio-plugin]WARNING: Odd aka. \"&tple\" & even aka. \"&tplo\" template haven't been defined"
                else
                  evenTpl = attrs['configs']['tple']
                  oddTpl = attrs['configs']['tplo']

                  # Load even and odd template from database
                  even = Metadata.first({ :conditions => ['key = ? and cat = ? and sites_id = ?',
                      evenTpl, 'placeholder', exts[:appid]
                  ]})

                  odd = Metadata.first({ :conditions => ['key = ? and cat = ? and sites_id = ?',
                      oddTpl, 'placeholder', exts[:appid]
                  ]})

                  if (!odd.nil? && !even.nil?)
                    oddContent = odd.value.strip
                    evenContent = even.value.strip
                    rCount = 0

                    contentObjs.each do |obj|
                      parsedObjs = Array.new
                      objectContent = ''

                      if (rCount % 2 == 0)
                        parsedObjs = evenContent.scan(/\[\[\$[a-zA-Z\-]+\]\]/)
                        objectContent = evenContent
                      else
                        parsedObjs = oddContent.scan(/\[\[\$[a-zA-Z\-]+\]\]/)
                        objectContent = oddContent
                      end

                      # logger.debug(parsedObjs.inspect)

                      parsedObjs.each do |pobj|
                        tpObj = pobj.gsub(/[^a-zA-Z\-]/, '')

                        # logger.debug(obj[tpObj].inspect)

                        if (!obj[tpObj].nil?)
                          objectContent = objectContent.gsub(pobj, obj[tpObj])
                        else
                          objectContent = objectContent.gsub(pobj, '')
                        end

                      end

                      contentResult << objectContent
                      rCount += 1
                    end

                  else
                    return "{{[rxcms-podio-plugin]WARNING: Either even, odd or both templates are missing}}"
                  end
                end

              elsif (attrs['mode'] == 'single')

                contentObjs.each do |obj|
                  objectContent = placeholderTemplate.strip
                  parsedObjs = objectContent.scan(/\[\[\$[a-zA-Z\-]+\]\]/)

                  parsedObjs.each do |pobj|
                    tpObj = pobj.gsub(/[^a-zA-Z\-]/, '')

                    if (!obj[tpObj].nil?)
                      objectContent = objectContent.gsub(pobj, obj[tpObj])
                    else
                      objectContent = objectContent.gsub(pobj, '')
                    end
                  end

                  contentResult << objectContent
                end

              else

                return "{{[rxcms-podio-plugin]WARNING: The mode \"#{attrs['mode']}\" is not supported}}"

              end

              contentResult.html_safe
            else
              placeholder.value.strip.html_safe
            end
          else
            placeholder.value.strip.html_safe
          end

        else
          placeholder.value.strip.html_safe
        end
      else
        placeholder.value.strip.html_safe
      end
    end

    # Input: podio app id, arrays of string and a set of attributes
    # Output: an empty array or an array of data
    def self.get_podio_items(podioAppId, fields = [], attrs = {
        :order => "ASC",
        :limit => 30,
        :offset => 0
    })
      appid = podioAppId.to_i
      order = attrs[:order].nil? ? "ASC" : attrs[:order].strip
      limit = attrs[:limit].nil? ? 30 : attrs[:limit]
      offset = attrs[:offset].nil? ? 0 : attrs[:offset]

      if (fields.length > 0)
        appFieldsArray = Array.new
        fields.each do |t|
          tHash = Hash.new

          tHash[:external_id] = t
          tHash[:simple] = true

          appFieldsArray << tHash
        end

        data = nil
        if (defined?(RxcmsPodioPlugin))
          data = AbstractItem.range(appid, appFieldsArray, {:order => order, :offset => offset, :limit => limit})
        end

        if (!data.nil?)
          return data
        else
          return []
        end
      else
        return []
      end
    end
  end

end