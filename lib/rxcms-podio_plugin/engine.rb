module RxcmsPodioPlugin
  class Engine < ::Rails::Engine

    config.generators do |g|
      g.test_framework :rspec
      # g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    config.autoload_paths += Dir["#{config.root}/lib/rxcms-podio_plugin/classes/**/"]

  end
end
