$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rxcms-podio_plugin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rxcms-podio_plugin"
  s.version     = RxcmsPodioPlugin::VERSION
  s.authors     = ["Anh Nguyen","Julian Cowan"]
  s.email       = ["rxcms@ngonluakiko.com"]
  s.homepage    = "https://bitbucket.org/kikoflame/rxcms-podio_plugin"
  s.summary     = "Connector for Podio.com"
  s.description = "Extension allows the site administrator to pull content from Podio.com"
  s.licenses    = ["MIT"]

  s.metadata = {
      "compatCode" => "2dd89a9ecdeb920d539954fd9fb3da7de9f52a61ee9895d06e4bc1bab11a598b",
      "fullName" => "Podio Integration",
      "mountPoint" => "/podio/engine/configure",
      "installerPoint" => "/podio/engine/installer"
  }

  s.files = Dir["{app,config,db,lib}/**/*"] + ["LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.13"
  s.add_dependency "podio"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "podio"

end
