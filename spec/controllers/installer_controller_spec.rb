require 'spec_helper'

describe InstallerController do

  describe "POST #before_process" do
    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    subject { post :before_process, { :serviceAccountName => 'podio@podio.com',
                                      :serviceAccountPass => '123456',
                                      :podioApiKey => '123456',
                                      :podioSecretKey => '123456'
    } }

    it "should process initially" do
      stub_const("UserSession", double())
      stub_const("User", double())
      stub_const("Metadata", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')
      SymmetricEncryption.should_receive(:load!).at_least(:once).and_return true
      Metadata.stub(:first) {Metadata}
      Metadata.should_receive(:destroy).at_least(:once).and_return true
      Metadata.should_receive(:create).at_least(:once).and_return true
      SymmetricEncryption.should_receive(:encrypt).at_least(:once).and_return true

      subject

      parsed = JSON.parse(response.body)
      expect(parsed['status']).to eq('success')
    end

    it "should fail to process initially when symmetric encryption fails" do
      stub_const("UserSession", double())
      stub_const("User", double())
      stub_const("Metadata", double())

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')

      subject

      parsed = JSON.parse(response.body)
      expect(parsed['status']).to eq('failure')
    end

    it "should fail to process initially when user is not logged in" do
      stub_const("UserSession", double())
      stub_const("User", double())

      expect { subject }.to raise_error
    end
  end

  describe "POST #core_process" do
    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    subject { post :core_process }

    it "should process core functions" do
      stub_const("UserSession", double())
      stub_const("User", double())

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')

      subject

      parsed = JSON.parse(response.body)
      expect(parsed['status']).to eq('unimplemented')
    end
  end

  describe "POST #post_process" do
    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    subject { post :post_process }

    it "should process post functions" do
      stub_const("UserSession", double())
      stub_const("User", double())

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')

      subject

      parsed = JSON.parse(response.body)
      expect(parsed['status']).to eq('unimplemented')
    end
  end

  describe "POST #uninstall" do
    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    subject { post :uninstall }

    it "should uninstall successfully" do
      stub_const("UserSession", double())
      stub_const("User", double())
      stub_const("Metadata", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')

      # An example of how to chain stub and should_receive
      Metadata.stub(:first) { Metadata }
      Metadata.should_receive(:destroy).at_least(:once).and_return true

      subject

      parsed = JSON.parse(response.body)
      expect(parsed['status']).to eq('success')
    end

    it "should fail to uninstall if user is not logged in" do
      stub_const("UserSession", double())
      stub_const("User", double())

      expect { subject }.to raise_error
    end
  end

end