require 'spec_helper'

describe SessionsController do

  before do

  end

  after do

  end

  it "should create podio session successfully" do
    TPodioClient = Struct.new(:client)
    TOauthTokenParent = Struct.new(:oauth_token)
    TOauthToken = Struct.new(:access_token, :refresh_token)
    podioClient = TPodioClient.new(TOauthTokenParent.new(TOauthToken.new('123','123')))

    stub_const('Metadata', double())
    stub_const('Podio', podioClient)

    TMetadata = Struct.new(:value)
    metadata = TMetadata.new('value')

    SymmetricEncryption.should_receive(:load!).and_return true
    Metadata.stub(:where) { Metadata }
    Metadata.should_receive(:first).at_least(:once).and_return metadata
    Podio.should_receive(:setup).and_return true
    SymmetricEncryption.should_receive(:decrypt).and_return 'someencryption'
    Podio.client.should_receive(:authenticate_with_credentials).and_return true
    Digest::SHA2.should_receive(:hexdigest).and_return '1234567ABCDEF'

    get :create_session_as_user
    response.code.should == '302'
  end

  it "should change session as user successfully" do
    TPodioClient = Struct.new(:client)
    TOauthTokenParent = Struct.new(:oauth_token)
    TOauthToken = Struct.new(:access_token, :refresh_token)
    podioClient = TPodioClient.new(TOauthTokenParent.new(TOauthToken.new('123','123')))

    stub_const('Metadata', double())
    stub_const('Podio', podioClient)

    TMetadata = Struct.new(:value)
    metadata = TMetadata.new('value')

    SymmetricEncryption.should_receive(:load!).and_return true
    Metadata.stub(:where) { Metadata }
    Metadata.should_receive(:first).at_least(:once).and_return metadata
    Podio.should_receive(:setup).and_return true
    SymmetricEncryption.should_receive(:decrypt).and_return 'someencryption'
    Podio.client.should_receive(:authenticate_with_credentials).and_return true
    Digest::SHA2.should_receive(:hexdigest).and_return '1234567ABCDEF'

    get :change_session_as_user
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

end
