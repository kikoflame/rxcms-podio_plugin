require 'spec_helper'

describe PodioController do

  before do
   controller.stub(:ensure_login).and_return true
  end

  after do

  end

  it "should be able to get podio items" do
    AbstractItem.should_receive(:range).and_return []
    get :get_podio_items, {:appid => '123', :fields => 'title'}

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should return nothing if there's a problem with podio" do
    AbstractItem.should_receive(:range).and_return nil
    get :get_podio_items, {:appid => '123', :fields => 'title'}

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should get a list of applications from workspace" do
    stub_const("UserSession", double())
    stub_const("User", double())

    TApp = Struct.new(:id,:config)
    apps = [TApp.new('1', {'name' => 'name', 'description' => 'description', 'icon' => 'icon'})]

    AbstractApplication.should_receive(:get_apps_list_by_space).and_return apps

    get :get_list_of_apps_from_workspace, { :space => '123456' }

    response.body.should_not be_nil
  end

  it "should check user login" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:test_authentication?).and_return true

    get :check_user_login, {:user => 'someuser', :pass => 'somepass'}

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should not get podio application status if there's an exception" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:app_exists?) { raise 'podio_error' }

    get :get_podio_app_status, {:appid => '123456'}

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should get podio application status" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:app_exists?).and_return true

    get :get_podio_app_status, {:appid => '123456'}

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should get list of applications for elements" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    TCurWorkspace = Struct.new(:value)
    curWorkspace = TCurWorkspace.new('value')

    TApp = Struct.new(:id,:value)
    tApps = [TApp.new('1','value'),TApp.new('2','value'), TApp.new('3','value')]

    Metadata.should_receive(:find_by_key).and_return curWorkspace
    AbstractApplication.should_receive(:get_apps_list_by_space).and_return tApps
    Metadata.should_receive(:all).and_return tApps

    get :get_list_of_apps_for_elements

    response.body.should_not be_empty
  end

  it "should not get a list of applications from workspace if there's a problem with podio" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:get_apps_list_by_space) { raise 'podio_error' }

    get :get_list_of_apps_from_workspace, { :space => '123456' }

    response.body.should_not be_empty
  end

  it "should not get list of applications for elements if metadata key doesn't exist" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    Metadata.should_receive(:find_by_key).and_return nil

    get :get_list_of_apps_for_elements

    response.body.should == '[]'
  end

  it "should not check user login if there's a problem with podio" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:test_authentication?).and_return false

    get :check_user_login, {:user => 'someuser', :pass => 'somepass'}

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should not get podio application status if there's a problem with podio" do
    stub_const("UserSession", double())
    stub_const("User", double())

    AbstractApplication.should_receive(:app_exists?).and_return false

    get :get_podio_app_status, {:appid => '123456'}

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
    parsed['message'].should  == 'offline'
  end

  it "should get a list of organizations of users" do
    stub_const("UserSession", double())
    stub_const("User", double())

    orgs = [{:name => 'name', :spaces => [{:id => '1', :name => 'some name'}]},{:name => 'name 2', :spaces => []}]

    AbstractOrganization.should_receive(:get_list_of_organizations).and_return orgs

    get :get_list_of_organizations_of_user

    response.body.should_not be_nil
  end

end
