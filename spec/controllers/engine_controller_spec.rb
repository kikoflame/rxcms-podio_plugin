require 'spec_helper'

describe EngineController do

  describe "GET #index" do

    it "should render index page" do
      get :index
      response.should be_success
    end

  end

  describe "GET #configure" do

    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    it "should render configure page" do
      stub_const("UserSession", double())
      stub_const("User", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      controller.stub(:ensure_login).and_return true
      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')
      controller.request.should_receive(:xhr?).at_least(:once).and_return true

      get :configure
      response.code.should == '406'
    end

    it "should deny access to configure if request is not xhr" do
      stub_const("UserSession", double())
      stub_const("User", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      controller.stub(:ensure_login).and_return true
      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')
      controller.request.should_receive(:xhr?).at_least(:once).and_return false

      expect { get :configure }.to raise_error
    end

  end

  describe "GET #installer" do

    before :all do
      TStructRecord = Struct.new(:record)
      TStructId = Struct.new(:id)
      TStructUser = Struct.new(:id,:login,:has_role?) do
        def has_role?(x,y)
          true
        end
      end
    end

    after :all do

    end

    it "should render installer page" do
      stub_const("UserSession", double())
      stub_const("User", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')
      controller.request.should_receive(:xhr?).at_least(:once).and_return true

      get :installer
      response.code.should == '406'
    end

    it "should deny access to installer if request is not xhr" do
      stub_const("UserSession", double())
      stub_const("User", double())

      stub_const("SiteUser", double())
      stub_const("Role", double())
      RoleStruct = Struct.new(:name)
      SiteUserStruct = Struct.new(:roles_id)

      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

      UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
      User.should_receive(:find).and_return TStructUser.new(1,'admin')
      controller.request.should_receive(:xhr?).at_least(:once).and_return false

      expect { get :installer }.to raise_error
    end
  end

end
