require 'spec_helper'

describe ServicesController do

  before :all do
    TStructRecord = Struct.new(:record)
    TStructId = Struct.new(:id)
    TStructUser = Struct.new(:id,:login,:has_role?) do
      def has_role?(x,y)
        true
      end
    end
  end

  after :all do

  end

  it "should set current podio workspace" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first).and_return Metadata
    Metadata.should_receive(:update).and_return Metadata

    post :set_current_podio_workspace, { :space => '123456' }

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should create default podio workspace if it doesn't exist" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first).and_return nil
    Metadata.stub(:create) { Metadata }
    Metadata.should_receive(:save).and_return Metadata

    post :set_current_podio_workspace, { :space => '123456' }

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should not do anything if there's a problem when trying to retrieve default podio workspace" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first) { raise 'podio_error' }

    post :set_current_podio_workspace, { :space => '123456' }

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should get current podio workspace" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    TCurWorkspace = Struct.new(:value)
    curWorkspace = TCurWorkspace.new('value')

    Metadata.should_receive(:first).at_least(:once).and_return curWorkspace

    controller.stub(:get_current_user_role).and_return 'admin'

    get :get_current_podio_workspace

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should fail to get current podio workspace" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first).and_return nil

    get :get_current_podio_workspace

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should fail to get current podio workspace if there's a problem" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first) { raise "podio_error" }

    get :get_current_podio_workspace

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should get current podio user" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    TCurAcc = Struct.new(:value)
    curAcc = TCurAcc.new('value')

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first).and_return curAcc

    get :get_current_podio_user

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should fail to get current podio user" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first).and_return nil

    get :get_current_podio_user

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should fail to get current podio user if there's a problem" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    Metadata.should_receive(:first) { raise "podio_error" }

    get :get_current_podio_user

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

  it "should update podio account" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    TPodioUser = Struct.new(:id)

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:encrypt).and_return '123466'
    Metadata.stub(:where) { Metadata }
    Metadata.should_receive(:first).at_least(:once).and_return TPodioUser.new('1')
    Metadata.should_receive(:update).at_least(:once).and_return Metadata

    post :update_or_create_podio_service_account, { :userObject => 'someobj', :passObject => {'value' => 'somevalue'}}

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should create podio account" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    TPodioUser = Struct.new(:id)

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    SymmetricEncryption.should_receive(:load!).and_return true
    SymmetricEncryption.should_receive(:encrypt).and_return '123466'
    Metadata.stub(:where) { Metadata }
    Metadata.should_receive(:first).at_least(:once).and_return nil
    Metadata.stub(:create) { Metadata }
    Metadata.should_receive(:save!).at_least(:once).and_return true

    post :update_or_create_podio_service_account, { :userObject => {'value' => 'somevalue', 'cat' => 'somecat', 'mime' => 'somemime'}, :passObject => {'value' => 'somevalue', 'cat' => 'somecat', 'mime' => 'somemime'}}

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should not do anything if there's an exception" do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())

    stub_const("SiteUser", double())
    stub_const("Role", double())
    RoleStruct = Struct.new(:name)
    SiteUserStruct = Struct.new(:roles_id)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).at_least(:once).and_return RoleStruct.new('admin')

    UserSession.should_receive(:find).at_least(:once).and_return TStructRecord.new(TStructId.new(1))
    User.should_receive(:find).and_return TStructUser.new(1,'admin')
    SymmetricEncryption.should_receive(:load!).and_return true

    Metadata.should_receive(:where) { raise 'podio_error' }

    post :update_or_create_podio_service_account, { :userObject => {'value' => 'somevalue', 'cat' => 'somecat', 'mime' => 'somemime'}, :passObject => {'value' => 'somevalue', 'cat' => 'somecat', 'mime' => 'somemime'}}

    parsed = JSON.parse(response.body)
    parsed['status'].should == 'failure'
  end

end
