require 'spec_helper'

describe RxcmsPodioPlugin::Executor do

  describe "#it should execute" do
    before do
      stub_const('Metadata', double())
      stub_const('AbstractApplication', double())
      stub_const('AbstractItem', double())

      TMetadataStruct = Struct.new(:MetadataAssociation, :value)
      TMetadataAssociationStruct = Struct.new(:value, :destId, :srcId)
      @metadata = TMetadataStruct.new([TMetadataAssociationStruct.new("true",1,2)], "<div>[[$title]]</div>")
      @podio = TMetadataStruct.new([], "123456[[$title]]")
    end

    it "should not perform execution when config attribute is malformed" do
      RxcmsPodioPlugin::Executor.execute(@metadata,{}, { :language => 'en' }).should_not be_nil
    end

    it "should not perform execution when config items attribute is malformed" do
      RxcmsPodioPlugin::Executor.execute(@metadata,{'configs' => 'zzz'}, { :language => 'en' }).should_not be_nil
    end

    it "should execute successfully if mode is single" do
      Metadata.should_receive(:find).at_least(:once).and_return @podio
      AbstractApplication.should_receive(:app_exists?).at_least(:once).and_return true
      AbstractItem.should_receive(:range).at_least(:once).and_return [{'title' => 'one'},{'title' => 'two'}]
      RxcmsPodioPlugin::Executor.execute(@metadata,{'configs' => {'items' => 'title|description'}, 'mode' => 'single'}, { :language => 'en' }).should_not be_nil
    end

    it "should execute successfully if mode is multiple" do
      Metadata.should_receive(:find).at_least(:once).and_return @podio
      AbstractApplication.should_receive(:app_exists?).at_least(:once).and_return true
      AbstractItem.should_receive(:range).at_least(:once).and_return [{'title' => 'one'},{'title' => 'two'}]
      RxcmsPodioPlugin::Executor.execute(@metadata,{'configs' => {'items' => 'title|description'}, 'mode' => 'multiple'}, { :language => 'en' }).should_not be_nil
    end

    it "should output warning when executing if mode is unsupported" do
      Metadata.should_receive(:find).at_least(:once).and_return @podio
      AbstractApplication.should_receive(:app_exists?).at_least(:once).and_return true
      AbstractItem.should_receive(:range).at_least(:once).and_return [{'title' => 'one'},{'title' => 'two'}]
      RxcmsPodioPlugin::Executor.execute(@metadata,{'configs' => {'items' => 'title|description'}, 'mode' => 'somethingelse'}, { :language => 'en' }).should_not be_nil
    end

    it "should execute successfully if mode is alternate" do
      Metadata.should_receive(:find).at_least(:once).and_return @podio
      Metadata.should_receive(:first).at_least(:once).and_return TMetadataStruct.new(nil,'name')
      AbstractApplication.should_receive(:app_exists?).at_least(:once).and_return true
      AbstractItem.should_receive(:range).at_least(:once).and_return [{'title' => 'one'},{'title' => 'two'}]
      RxcmsPodioPlugin::Executor.execute(@metadata,{'configs' => {'items' => 'title|description', 'tple' => 'even', 'tplo' => 'odd'}, 'mode' => 'alternate'}, { :language => 'en' }).should_not be_nil
    end
  end

end