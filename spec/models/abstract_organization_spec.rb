require 'spec_helper'

describe AbstractOrganization do

  before do

  end

  after do

  end

  it "should find all organizations from podio" do
    AbstractOrganization.should_receive(:find_all).and_return [1,2,3]
    AbstractOrganization.send(:get_list_of_organizations).should_not be_empty
  end

  it "should not find all organizations from podio if there's a problem with podio" do
    AbstractOrganization.should_receive(:find_all) { raise "podio error" }
    AbstractOrganization.send(:get_list_of_organizations).should be_empty
  end

end
