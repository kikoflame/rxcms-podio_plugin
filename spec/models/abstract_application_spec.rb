require 'spec_helper'

describe AbstractApplication do

  before :all do

  end

  after :all do

  end

  it "should get list of applications from space" do
    Podio::Application.should_receive(:find_all_for_space).and_return [1,2,3]
    AbstractApplication.send(:get_apps_list_by_space, 123).should_not be_empty
  end

  it "should not get list of applications from space if there's error" do
    Podio::Application.should_receive(:find_all_for_space) { raise 'podio_error'}
    AbstractApplication.send(:get_apps_list_by_space, 123).should be_empty
  end

  it "should test authentication" do
    Podio.client.should_receive(:authenticate_with_credentials).and_return true
    AbstractApplication.send(:test_authentication?, '123', '123').should be_true
  end

  it "should fail to test authentication when there's a problem" do
    Podio.client.should_receive(:authenticate_with_credentials) { raise 'podio_error'}
    AbstractApplication.send(:test_authentication?, '123', '123').should be_false
  end

  it "should be able to check if application exists" do
    Podio::Application.should_receive(:find).and_return true
    AbstractApplication.send(:app_exists?, '123').should be_true
  end

  it "should not be able to check if application exists when there's a problem" do
    Podio::Application.should_receive(:find) { raise 'podio_error'}
    AbstractApplication.send(:app_exists?, '123').should be_false
  end

end
