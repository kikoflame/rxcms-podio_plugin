require 'spec_helper'

describe AbstractItem do

  before :all do
    TStructPodioItem = Struct.new(:attributes)
  end

  after :all do

  end

  it "should find all items in app" do
    item1 = TStructPodioItem.new({:item_id => 1, :fields => [Hash['external_id','title','values',[{'value' => 'value1'}]]]})
    item2 = TStructPodioItem.new({:item_id => 2, :fields => [Hash['external_id','title','values',[{'value' => 'value2'}]]]})

    AbstractApplication.should_receive(:app_exists?).and_return true
    AbstractItem.should_receive(:find_by_filter_values).and_return Hash[:all, [item1,
                                                                               item2
                                                                               ]]
    AbstractItem.send(:range, 123456, [{:external_id => 'title', :simple => true}]).should_not be_nil
  end

  it "should not find all items in app if it doesn't exist" do
    item1 = TStructPodioItem.new({:item_id => 1, :fields => [Hash['external_id','title','values',[{'value' => 'value1'}]]]})
    item2 = TStructPodioItem.new({:item_id => 2, :fields => [Hash['external_id','title','values',[{'value' => 'value2'}]]]})

    AbstractApplication.should_receive(:app_exists?).and_return false
    expect { AbstractItem.send(:range, 123456, [{:external_id => 'title', :simple => true}]) }.to raise_error
  end

  it "should not find all items if the access to app is restricted" do
    item1 = TStructPodioItem.new({:item_id => 1, :fields => [Hash['external_id','title','values',[{'value' => 'value1'}]]]})
    item2 = TStructPodioItem.new({:item_id => 2, :fields => [Hash['external_id','title','values',[{'value' => 'value2'}]]]})

    AbstractApplication.should_receive(:app_exists?).and_return true
    AbstractItem.should_receive(:find_by_filter_values) { raise "podio error" }
    expect { AbstractItem.send(:range, 123456, [{:external_id => 'title', :simple => true}]) }.to raise_error
  end

end
