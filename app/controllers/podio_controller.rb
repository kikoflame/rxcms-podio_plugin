class PodioController < ApplicationController
  layout false

  before_filter :ensure_login

  @@abbrErr = "PCR"

  # Get items of apps from podio
  # Inputs: podio application id, order, limit, offset and fields which have the forms described below
  # Output: valid json string
  def get_podio_items

    appid = params[:appid].to_i
    order = params[:order].nil? ? "ASC" : params[:order].strip # string default = "ASC"
    limit = params[:limit].nil? ? 30 : params[:limit].to_i # numeric default = 30
    offset = params[:offset].nil? ? 0 : params[:offset].to_i # numeric default = 0
    fields = params[:fields].strip # title|money|summary

    appFields = fields.split("|")

    appFieldsArray = Array.new
    appFields.each do |t|
      tHash = Hash.new

      tHash[:external_id] = t
      tHash[:simple] = true

      appFieldsArray << tHash
    end

    data = nil

    data = get_abstract_items(appFieldsArray, appid, limit, offset, order)

    if (!data.nil?)
      render :json => {:status => "success", :data => JSON.parse(data.to_json)}
    else
      render :json => {:status => "failure"}
    end
  end

  def get_abstract_items(appFieldsArray, appid, limit, offset, order)
    data = AbstractItem.range(appid, appFieldsArray, {:order => order, :offset => offset, :limit => limit})
    return data
  end

  # Get status of podio
  # Input from GET['appid']
  # Output json string
  def get_podio_app_status
    begin
      data = params[:appid]
      app = AbstractApplication.app_exists?(data.to_i)
      if (app == true)
        render :json => {:status => "success", :message => "online"}
      else
        render :json => {:status => "success", :message => "offline"}
      end
    rescue Exception => ex
      render :json => {:status => "failure", :message => "error"}
    end
  end

  # Get authentication status of podio account
  # Input from POST['user'], POST['pass']
  # Output json string
  def check_user_login
    podioUser = params[:user]
    podioPass = params[:pass]
    status = AbstractApplication.test_authentication?(podioUser, podioPass)

    if (status)
      render :json => {:status => "success"}
    else
      render :json => {:status => "failure"}
    end
  end

  # Get list of organizations of user
  # Input
  # Output
  def get_list_of_organizations_of_user
    orgs = AbstractOrganization.get_list_of_organizations

    orgsArray = Array.new
    orgs.each do |o|
      tHash = Hash.new
      tArray = Array.new

      tHash[:id] = defined?(o.id) ? o.id : -1
      tHash[:name] = o[:name]

      o[:spaces].each do |s|
        tSpaceHash = Hash.new

        tSpaceHash[:id] = s['space_id']
        tSpaceHash[:name] = s['name']
        tArray << tSpaceHash
      end
      tHash[:spaces] = tArray

      orgsArray << tHash
    end

    render :json => orgsArray
  end

  # Get list of workspaces of user
  # Input
  # Output
  def get_list_of_apps_from_workspace
    begin
      spaceid = params[:space]

      appsObj = AbstractApplication.get_apps_list_by_space(spaceid)

      apps = Array.new
      appsObj.each do |a|
        tHash = Hash.new
        tHash['id'] = defined?(a.id) ? a.id : -1
        tHash['name'] = defined?(a.config) ? a.config['name'] : ''
        tHash['description'] = defined?(a.config) ? a.config['description'] : ''
        tHash['icon'] = defined?(a.config) ? a.config['icon'] : ''

        apps << tHash
      end

      render :json => apps
    rescue
      render :json => []
    end
  end

  # Get list of apps for elements
  # Input
  # Output json string
  def get_list_of_apps_for_elements
    # Get all apps
    curWorkspace = Metadata.find_by_key("currentWorkspace")
    if (!curWorkspace.nil?)
      appList = AbstractApplication.get_apps_list_by_space(curWorkspace.value.strip)

      tApps = Metadata.all({:conditions => ['cat = ?', 'app']})

      tAppsArray = Array.new
      appList.each do |al|
        tAppsArray << (defined?(al.id) ? al.id.to_s : (-1).to_s)
      end

      tAppsResult = Array.new
      tApps.each do |ta|
        if (tAppsArray.include?(ta.value.strip))
          tAppsResult << ta
        end
      end

      apps = tAppsResult
    else
      apps = []
    end

    render :json => apps
  end

  protected

  # PODIO plugin
  def ensure_login
    if session[:podio_access_token]
      # Get service user account information from database
      usrName = Metadata.where("key = ?", "serviceAccountName").first
      usrPass = Metadata.where("key = ?", "serviceAccountPass").first

      if (!usrName.nil? && !usrPass.nil?)
        if cookies[:podio].to_s == Digest::SHA2.hexdigest("#{usrName.value.strip}#{usrPass.value.strip}")
          init_podio_client
        else
          redirect_to "/podio/auth/podio_as_user"
        end
      else
        raise "Application - #{@@abbrErr}001 Since you use podio integration plugin, a service account must be used"
      end
    else
      redirect_to "/podio/auth/podio_as_user"
    end
  end

  def init_podio_client
    apiKey = Metadata.first({:conditions => ["key = ?", "podioApiKey"]
                            })
    secretKey = Metadata.first({:conditions => ["key = ?", "podioSecretKey"]
                               })
    if (!apiKey.nil? && !secretKey.nil?)
      Podio.setup(
          :api_url => 'https://api.podio.com',
          :api_key => apiKey.value.strip,
          :api_secret => secretKey.value.strip,
          :oauth_token => Podio::OAuthToken.new('access_token' => session[:podio_access_token], 'refresh_token' => session[:podio_refresh_token])
      )
    else
      # Don't do anything
    end
  end

end
