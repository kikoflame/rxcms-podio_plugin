class SessionsController < ApplicationController
  layout nil

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/podio/podio_config.yml', __FILE__))))

  # Create_Session as user action
  def create_session_as_user
    begin
      SymmetricEncryption.load!

      constServiceAccountInfoKey = APP_CONFIG[:SERVICE_ACCOUNT_NAME]
      constServiceAccountPassKey = APP_CONFIG[:SERVICE_ACCOUNT_PASS]

      apiKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid]]
                              })
      secretKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]]
                                 })

      # Get service user account information from database
      usrName = Metadata.where("key = ? and sites_id = ?", constServiceAccountInfoKey, session[:accessible_appid]).first
      usrPass = Metadata.where("key = ? and sites_Id = ?", constServiceAccountPassKey, session[:accessible_appid]).first

      # API key, secret key, user and password credentials are required to proceed; if not, an error is raised
      if (!usrName.nil? && !usrPass.nil? && !apiKey.nil? && !secretKey.nil?)

        Podio.setup(
            :api_url => 'https://api.podio.com',
            :api_key => apiKey.value.strip,
            :api_secret => secretKey.value.strip
        )

        # Authenticate using user ID
        Podio.client.authenticate_with_credentials(usrName.value.strip, SymmetricEncryption.decrypt(usrPass.value.strip))

        # Store authentication session variables
        session[:podio_access_token] = Podio.client.oauth_token.access_token
        session[:podio_refresh_token] = Podio.client.oauth_token.refresh_token

        # Store credential hash as cookies
        cookies.delete(:podio)
        cookies[:podio] = Digest::SHA2.hexdigest("#{usrName.value.strip}#{usrPass.value.strip}")

        if (!cookies[:url].nil? && !cookies[:url].empty?)
          redirect_to cookies[:url].to_s
        else
          redirect_to "/"
        end
      else
        raise
      end

    rescue Exception => ex

      raise "#{ex.message}"

    end
  end

  # End of Create_Session as user action
  def change_session_as_user
    begin
      SymmetricEncryption.load!

      constServiceAccountInfoKey = APP_CONFIG[:SERVICE_ACCOUNT_NAME]
      constServiceAccountPassKey = APP_CONFIG[:SERVICE_ACCOUNT_PASS]

      apiKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid]]})
      secretKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]]})

      # Get service user account information from database
      usrName = Metadata.where("key = ? and sites_id = ?", constServiceAccountInfoKey, session[:accessible_appid]).first
      usrPass = Metadata.where("key = ? and sites_id = ?", constServiceAccountPassKey, session[:accessible_appid]).first

      if (!usrName.nil? && !usrPass.nil? && !apiKey.nil? && !secretKey.nil?)
        Podio.setup(
            :api_url => 'https://api.podio.com',
            :api_key => apiKey.value.strip,
            :api_secret => secretKey.value.strip
        )

        # Authenticate using user ID
        Podio.client.authenticate_with_credentials(usrName.value.strip, SymmetricEncryption.decrypt(usrPass.value.strip))

        # Store authentication session variables
        session[:podio_access_token] = Podio.client.oauth_token.access_token
        session[:podio_refresh_token] = Podio.client.oauth_token.refresh_token

        # Store credential hash as cookies
        cookies.delete(:podio)
        cookies[:podio] = Digest::SHA2.hexdigest("#{usrName.value.strip}#{usrPass.value.strip}")

        render :json => {:status => "success"}
      else
        render :json => {:status => "failure"}
      end

    rescue Exception => ex

      render :json => {:status => "failure"}

    end
  end

  # Success_Callback, will be called if a session is successfully created
  # def success_callback

  #  render :text => 'Session created!'

  # end

  # Delete_Session action, will be called if a session is successfully cleared
  # def delete_session

  #  session.clear
  #  render :text => 'Session cleared!'

  # end

end