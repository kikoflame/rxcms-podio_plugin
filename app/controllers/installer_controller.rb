class InstallerController < ApplicationController
  include PluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/podio/podio_config.yml', __FILE__))))

  # Each step should return JSON status "success", "failure" or "unimplemented"

  # Used for initializing and creating database entries
  def before_process
    begin
      SymmetricEncryption.load!

      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'anonymous' ||
          @curUserRole == 'loggedin')
        raise 'unauthorized access'
      end

      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]] }).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_PASS], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_PASS], session[:accessible_appid]] }).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid] ]}).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid] ]}).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]] }).destroy
      end

      currentWorkspaceMetadata = Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_WORKSPACE], session[:accessible_appid]] })
      if (!currentWorkspaceMetadata.nil?)
        currentWorkspaceMetadata.destroy
      end

      Metadata.create({
        :key => APP_CONFIG[:SERVICE_ACCOUNT_NAME],
        :value => params[:serviceAccountName].strip,
        :cat => 'podio_config',
        :mime => 'text/plain',
        :sites_id => session[:accessible_appid]
      })
      Metadata.create({
        :key => APP_CONFIG[:SERVICE_ACCOUNT_PASS],
        :value => SymmetricEncryption.encrypt(params[:serviceAccountPass].strip),
        :cat => 'podio_config',
        :mime => 'text/plain',
        :sites_id => session[:accessible_appid]
      })
      Metadata.create({
        :key => APP_CONFIG[:PODIO_API_KEY],
        :value => params[:podioApiKey].strip,
        :cat => 'podio_config',
        :mime => 'text/plain',
        :sites_id => session[:accessible_appid]
      })
      Metadata.create({
        :key => APP_CONFIG[:PODIO_SECRET_KEY],
        :value => params[:podioSecretKey].strip,
        :cat => 'podio_config',
        :mime => 'text/plain',
        :sites_id => session[:accessible_appid]
      })

      render :json => { :status => 'success' }
    rescue
      render :json => { :status => 'failure' }
    end
  end

  # Used for logical processing
  def core_process
    render :json => { :status => 'unimplemented' }
  end

  # Used for configuring data
  def post_process
    render :json => { :status => 'unimplemented' }
  end

  # Uninstaller
  def uninstall
    begin
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'anonymous' ||
          @curUserRole == 'loggedin')
        raise 'unauthorized access'
      end

      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]] }).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_PASS], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:SERVICE_ACCOUNT_PASS], session[:accessible_appid]] }).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid] ]}).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid] ]}).destroy
      end
      if (!Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]] }).nil?)
        Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]] }).destroy
      end

      currentWorkspaceMetadata = Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_WORKSPACE], session[:accessible_appid]] })
      if (!currentWorkspaceMetadata.nil?)
        currentWorkspaceMetadata.destroy
      end

      render :json => { :status => 'success' }
    rescue
      render :json => { :status => 'failure' }
    end
  end

  private

end