class ServicesController < ApplicationController
  include PluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/podio/podio_config.yml', __FILE__))))

  # Set current podio workspace
  # Input
  # Output
  def set_current_podio_workspace
    begin

      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'anonymous' ||
          @curUserRole == 'loggedin')
        raise 'unauthorized access'
      end

      space = params[:space]
      metaId = Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_WORKSPACE], session[:accessible_appid]] });

      if (!metaId.nil?)
        Metadata.update(metaId, { :value => space })
      else
        Metadata.create({
                            :key => "currentWorkspace",
                            :value => space,
                            :mime => "plain/text",
                            :cat => "podio_config",
                            :sites_id => session[:accessible_appid]
                        }).save
      end

      render :json => { "status" => "success" }
    rescue
      render :json => { "status" => "failure", "message" => "Unable to set current podio workspace" }
    end
  end

  # Get current podio workspace
  # Input
  # Output
  def get_current_podio_workspace
    begin

      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'anonymous' ||
          @curUserRole == 'loggedin')
        raise 'unauthorized access'
      end

      curWorkspace = Metadata.first({ :conditions => ['key = ? and sites_id = ?', APP_CONFIG[:PODIO_WORKSPACE], session[:accessible_appid]] })

      if (!curWorkspace.nil?)
        render :json => { "status" => "success", "data" => curWorkspace.value.to_s.strip }
      else
        render :json => { "status" => "failure" }
      end
    rescue
      render :json => { "status" => "failure" }
    end
  end

  # Get current podio service account user
  # Input
  # Output
  def get_current_podio_user
    begin

      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'anonymous' ||
          @curUserRole == 'loggedin')
        raise 'unauthorized access'
      end

      serviceAccount = Metadata.first({ :conditions => [
          "key = ? and sites_id = ?", APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]
      ]})

      if (!serviceAccount.nil?)
        render :json => { "status" => "success", "data" => serviceAccount.value.strip}
      else
        render :json => { "status" => "failure" }
      end
    rescue
      render :json => { "status" => "failure" }
    end
  end

  # Update or create podio service account
  # Input from POST['userObject'], POST['passObject']
  # Output json string
  def update_or_create_podio_service_account
    SymmetricEncryption.load!

    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise 'unauthorized access'
    end

    constServiceAccountInfoKey = APP_CONFIG[:SERVICE_ACCOUNT_NAME]
    constServiceAccountPassKey = APP_CONFIG[:SERVICE_ACCOUNT_PASS]

    podioUserObj = params[:userObject]
    podioPassObj = params[:passObject]

    begin
      fPodioUser = Metadata.where("key = ? and sites_id = ?", constServiceAccountInfoKey, session[:accessible_appid]).first
      fPodioPass = Metadata.where("key = ? and sites_id = ?", constServiceAccountPassKey, session[:accessible_appid]).first

      if (!fPodioUser.nil?)
        Metadata.update(fPodioUser.id, podioUserObj)
      else
        Metadata.create({
                            :key => constServiceAccountInfoKey,
                            :value => podioUserObj['value'],
                            :cat => podioUserObj['cat'],
                            :mime => podioUserObj['mime'],
                            :sites_id => session[:accessible_appid]
                        }).save!
      end

      if (!fPodioPass.nil?)
        podioPassObj['value'] = SymmetricEncryption.encrypt(podioPassObj['value'])
        Metadata.update(fPodioPass.id, podioPassObj)
      else
        Metadata.create({
                            :key => constServiceAccountPassKey,
                            :value => SymmetricEncryption.encrypt(podioPassObj['value']),
                            :cat => podioPassObj['cat'],
                            :mime => podioPassObj['mime'],
                            :sites_id => session[:accessible_appid]
                        }).save!
      end

      render :json => { :status => "success" }
    rescue Exception => ex
      render :json => { :status => "failure", :message => ex.message }
    end
  end

  private

end