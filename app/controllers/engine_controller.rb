class EngineController < ApplicationController
  include PluginHelper

  layout false

  before_filter :get_current_user_role, :except => [
      :index
  ]

  before_filter :ensure_login, :only => [ :configure ]

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/podio/podio_config.yml', __FILE__))))

  # Write your readme here
  def index

  end

  def configure
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  def installer
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'anonymous' ||
        @curUserRole == 'loggedin')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  protected

  # PODIO plugin
  def ensure_login
    if session[:podio_access_token]
      # Get service user account information from database
      usrName = Metadata.where("key = ? and sites_id = ?", APP_CONFIG[:SERVICE_ACCOUNT_NAME], session[:accessible_appid]).first
      usrPass = Metadata.where("key = ? and sites_id = ?", APP_CONFIG[:SERVICE_ACCOUNT_PASS], session[:accessible_appid]).first

      if (!usrName.nil? && !usrPass.nil?)
        if cookies[:podio].to_s == Digest::SHA2.hexdigest("#{usrName.value.strip}#{usrPass.value.strip}")
          init_podio_client
        else
          redirect_to "/podio/auth/podio_as_user"
        end
      else
        raise "[Err] Since you use podio integration plugin, a service account must be used"
      end
    else
      redirect_to "/podio/auth/podio_as_user"
    end
  end

  def init_podio_client
    apiKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_API_KEY], session[:accessible_appid]]
                            })
    secretKey = Metadata.first({:conditions => ["key = ? and sites_id = ?", APP_CONFIG[:PODIO_SECRET_KEY], session[:accessible_appid]]
                               })
    if (!apiKey.nil? && !secretKey.nil?)
      Podio.setup(
          :api_url => 'https://api.podio.com',
          :api_key => apiKey.value.strip,
          :api_secret => secretKey.value.strip,
          :oauth_token => Podio::OAuthToken.new('access_token' => session[:podio_access_token], 'refresh_token' => session[:podio_refresh_token])
      )
    else
      # Don't do anything
    end
  end

  private

end


