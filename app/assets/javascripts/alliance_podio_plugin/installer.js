$(function(){
    $("#submit").on("click", function(){

        if ($("#username").val().length > 0 &&
            $("#password").val().length > 0 &&
            $("#podioApiKey").val().length > 0 &&
            $("#podioSecretKey").val().length > 0)
        {
            $.post("/podio/installer/before_process", {
                    'serviceAccountName' : $("#username").val(),
                    'serviceAccountPass' : $("#password").val(),
                    'podioApiKey' : $("#podioApiKey").val(),
                    'podioSecretKey' : $("#podioSecretKey").val()
                },
                function(response)
                {
                    if (response.status == "success")
                        location.reload();
                    else
                        alert("There was an unresolvable problem!");
                });
        } else
            alert("Those fields are required!");

    });
});