var const_key_regex = /^[a-zA-Z\-]+$/i;
var const_value_regex = /^[0-9]+$/i

$(function () {
    var appToBeDelete = null;
    var curListOfAppsFromWorkspace = []
    var activeSystemCell = null;
    var curTab = null;

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    $("#systemTabs a[data-toggle=\"tab\"]").on("click", function (e) {
        var targetId = $(e.target).attr("href").replace("#", "");
        curTab = targetId;

        switch (targetId) {
            case "serviceAccountConfig":
                // Don't do anything at the moment
                $.get("/podio/user/current",function (response) {
                    if (response.status == "success") {
                        $("#serviceAccountStatus").text("Service account \"" + response.data.trim() + "\" is currently active");
                    }
                    else
                        $("#serviceAccountStatus").text("No service account are currently active");
                }).error(function () {
                        $("#serviceAccountStatus").text("No service account are currently active");
                    });
                break;
            case "podio":
                // Ask server to return a list of apps
                $("#podioTabLoadingText").fadeIn("fast");
                $("#appTable tBody:first > tr:gt(1)").remove();
                $("#newAppIdSelect").html($("<option value=\"\">loading...</option>"));

                console.log("Hit 1");
                $.getJSON("/manager/s/items/list/app",function (response) {

                    console.log("Hit 2");
                    // Populate all registered apps
                    if (response.length > 0)
                        for (var i = 0; i < response.length; i++) {
                            var cloned = $("#podioAppItemDummy").clone();
                            $($(cloned).find("td")[0]).attr("data-id", response[i].id);
                            $($(cloned).find("td")[0]).text((i + 1).toString());
                            $($($(cloned).find("td")[3]).find("span")[0]).text(response[i].key);
                            $($($(cloned).find("td")[4]).find("span")[0]).text(response[i].value);

                            $(cloned).removeAttr("id");
                            $(cloned).removeAttr("style");

                            $("#appTable tBody:first").append(cloned);
                        }
                    else
                        Messenger().post("No apps were registered!");

                    // Query server for current space and get a list of apps for it

                    $.getJSON("/podio/orgs/curspace",function (space) {
                        if (space.status == "success") {
                            console.log("Hit 3");
                            $.getJSON("/podio/orgs/space/" + space.data,function (data) {
                                if (data.length > 0) {
                                    console.log("Hit 4");
                                    for (var d = 0; d < data.length; d++) {
                                        curListOfAppsFromWorkspace = data;
                                        var appOptions = ""
                                        for (var i = 0; i < curListOfAppsFromWorkspace.length; i++) {
                                            var appOptionItem = "<option value=\"" + curListOfAppsFromWorkspace[i].id + "\">" + curListOfAppsFromWorkspace[i].name + "</option>";
                                            appOptions += appOptionItem;
                                        }
                                        $("#newAppIdSelect").html(appOptions);
                                    }
                                }
                                else {
                                    $("#newAppIdSelect").html("<option value=\"\">no apps found</option>")
                                }


                                $("#podioTabLoadingText").fadeOut("fast");
                            }).error(function () {
                                    $("#podioTabLoadingText").fadeOut("fast");
                                });
                        } else {
                            console.log("Hit 6");
                            Messenger().post("Default podio workspace wasn't set!");
                            $("#podioTabLoadingText").fadeOut("fast");
                            $("#newAppIdSelect").html($("<option value=\"\">error when retrieving</option>"))
                        }
                    }).error(function () {
                            Messenger().post("Default podio workspace wasn't set!");
                            $("#podioTabLoadingText").fadeOut("fast");
                            $("#newAppIdSelect").html($("<option value=\"\">error when retrieving</option>"))
                        });


                }).error(function () {
                        $("#podioTabLoadingText").fadeOut("fast");
                        $("#newAppIdSelect").html($("<option value=\"\">error when retrieving</option>"))
                    });

                break;
            default:

        }
    });

    //$("#systemTabs a:first").tab("show");

    $("#appTableBody").on("click", "tr.appItem td:nth-child(2) a", function (e) {
        appToBeDelete = $(this);
    });

    $("#appDeleteBtn").on("click", function () {
        itemCounts = 0;

        if (appToBeDelete != null) {
            var appDeleteId = $($($(appToBeDelete).parents("td")[0]).prev()).attr("data-id");

            //alert(appDeleteId);
            $.ajax({
                url: "/manager/configurations/" + appDeleteId,
                type: "DELETE",
                beforeSend: function () {
                    $("#overlay").fadeIn("fast");
                },
                error: function () {
                    $("#overlay").fadeOut("fast", function () {
                        Messenger().post("Cannot contact server!");
                    });
                },
                success: function (response) {
                    if (response.status == "success") {
                        $($(appToBeDelete).parents("tr")[0]).remove();

                        $("tr.appItem:visible").each(function (index) {
                            itemCounts++;
                            $($(this).find("td:first-child")).text(itemCounts);
                        });

                        $("#overlay").fadeOut("fast");
                        Messenger().post("App has just been unregistered");
                    }
                    else {
                        $("#overlay").fadeOut("fast");
                        Messenger().post("Failure - reason: " + response.message);
                    }
                }
            });
        }
    });

    $("#appDelete").on("hidden", function () {
        appToBeDelete = null;
    });

    $("#aNewApp").on("click", function (e) {
        var newAppName = $("#newAppNameTxt").val(),
            newAppId = $("#newAppIdSelect").val();

        if (const_key_regex.test(newAppName) && const_value_regex.test(newAppId)) {

            $.ajax({
                url: "/manager/configurations",
                type: "POST",
                data: {
                    "key": newAppName,
                    "value": newAppId,
                    "cat": "app",
                    "mime": "text/plain"
                },
                beforeSend: function () {
                    $("#overlay").fadeIn("fast");
                },
                error: function () {
                    $("#overlay").fadeOut("fast", function () {
                        Messenger().post("Cannot contact server!");
                    });
                },
                success: function (response) {

                    if (response.status == "success") {
                        var cloned = $("#podioAppItemDummy").clone();

                        var listOfAppItem = $("#appTableBody").find("tr:visible").filter(function (index) {
                            return $(this).hasClass("appItem");
                        });

                        var newId = 1;
                        if (listOfAppItem.length > 0)
                        {
                            var itemIds = new Array();
                            listOfAppItem.each(function (index) {
                                curId = parseInt($($(this).find("td")[0]).text());
                                itemIds.push(curId);
                            });

                            newId = Math.max.apply(Math, itemIds) + 1;
                        }

                        $($(cloned).find("td")[0]).text(newId.toString());
                        $($(cloned).find("td")[0]).attr("data-id", response.data.newid);
                        $($($(cloned).find("td")[3]).find("span:first-child")).text($("#newAppNameTxt").val());
                        $($($(cloned).find("td")[4]).find("span:first-child")).text($("#newAppIdSelect").val());

                        $("#newAppNameTxt").val("");
                        $("#newAppIdSelect").val("");

                        $(cloned).insertAfter($(".appItem:last"));
                        $(cloned).fadeIn("fast");

                        $("#overlay").fadeOut("fast", function () {
                            Messenger().post("Your app has just been registered!");
                        });
                    } else if (response.status == "failure") {
                        $("#overlay").fadeOut("fast", function () {
                            Messenger().post("Failure - reason: " + response.message);
                        });
                    }

                }
            });

        } else {
            Messenger().post("Your input data were not valid!");
        }

    });

    $("#activateServiceAccountBtn").on("click", function () {
        var uid = $("#username").val(),
            pass = $("#password").val();

        if (uid.length > 0 && pass.length > 0) {
            $("#orgsIndicator").fadeIn("fast");
            $.post("/podio/user/login", {
                    "user": uid,
                    "pass": pass
                },
                function (response) {
                    if (response.status == "success") {
                        $("#serviceAccountStatus").html("Service account \"" + uid + "\" is currently active");

                        var userInfoObject = {
                            "value": uid.trim(),
                            "cat": "config",
                            "mime": "text/plain"
                        }

                        var userPassObject = {
                            "value": pass.trim(),
                            "cat": "config",
                            "mime": "text/plain"
                        }

                        $.post("/podio/user/manipulate", {
                            "userObject": userInfoObject,
                            "passObject": userPassObject
                        },function (response) {
                            if (response.status == "success") {
                                var optOrgs = "<option value=\"\"></option>";
                                $.get("/podio/auth/podio_change_user",function (response) {
                                    if (response.status == "success") {
                                        $.getJSON("/podio/data/orgs",function (orgs) {
                                            for (var o = 0; o < orgs.length; o++) {
                                                optOrgs += "<option value=\"" + orgs[o].id.toString() + "\" >" + orgs[o].name.trim() + "</option>";
                                            }
                                            $("#selectOrganization").html(optOrgs.trim());
                                            $("#chooseOrganization").fadeIn("fast");
                                        }).error(function () {
                                                Messenger().post("Unable to retrieve a list of organization!");
                                            });
                                    } else {
                                        Messenger().post("Unable to switch service account!");
                                    }

                                    $("#orgsIndicator").fadeOut("fast");
                                }).error(function () {
                                        $("#orgsIndicator").fadeOut("fast");
                                        Messenger().post("Unable to change service account!");
                                    });
                            } else {
                                $("#orgsIndicator").fadeOut("fast");
                                Messenger().post("Unable to prepare service account!");
                            }
                        }).error(function () {
                                $("#orgsIndicator").fadeOut("fast");
                                Messenger().post("Unable to check service account!");
                            });

                        $("#username").val("");
                        $("#password").val("");
                    } else {
                        Messenger().post("Unable to authenticate you with podio!");
                        $("#orgsIndicator").fadeOut("fast");
                    }
                }).error(function () {
                    Messenger().post("Cannot contact server!");
                    $("#orgsIndicator").fadeIn("fast");
                });
        } else
            Messenger().post("Both podio username and passwords must not be blank!");
    });

    $("#selectOrganization").on("change", function () {
        var $this = $(this);

        var orgId = $this.val();

        if (orgId.length > 0) {
            $("#workspacesIndicator").fadeIn("fast");
            $("#chooseWorkspace").css("display", "none");
            $.getJSON("/podio/data/orgs",function (orgs) {
                var org = $.grep(orgs, function (elem, index) {
                    return elem.id == orgId
                });

                var optSpaces = "<option value=\"\"></option>";

                for (var s = 0; s < org[0].spaces.length; s++) {
                    optSpaces += "<option value=\"" + org[0].spaces[s].id.toString() + "\" >" + org[0].spaces[s].name.trim() + "</option>";
                }

                $("#selectPodioWorkspace").html(optSpaces.trim());
                $("#chooseWorkspace").fadeIn("fast");
                $("#workspacesIndicator").fadeOut("fast");
            }).error(function () {
                    $("#workspacesIndicator").fadeOut("fast");
                });
        } else
            $("#selectPodioWorkspace").html("");
    });

    $("#selectPodioWorkspace").on("change", function () {
        // Set current workspace on server
        $.post("/podio/orgs/space/manipulate", {
            "space": $(this).val()
        },function (response) {
            Messenger().post("New default workspace has just been set!");
        }).error(function () {
                Messenger().post("Unabled to set new default workspace!");
            });

        $.getJSON("/podio/orgs/space/" + $(this).val(),
            function (response) {
                if (response.length > 0) {
                    // Set current workspace on server
                    curListOfAppsFromWorkspace = response;
                }
            }).error(function () {
                Messenger().post("Unable to retrieve a list of apps for workspace!")
            });
    });

    $("#aAppAccesses").on("click", function () {
        // Initially load accessible apps in workspace
        $("#accessibleApps").html("<li>Looking up...</li>");
        $.getJSON("/podio/orgs/curspace",function (space) {
            if (space.status == "success") {
                $.getJSON("/podio/orgs/space/" + space.data,
                    function (response) {
                        if (response.length > 0) {
                            curListOfAppsFromWorkspace = response;

                            var accessibleAppListItems = "";
                            for (var a = 0; a < curListOfAppsFromWorkspace.length; a++) {
                                accessibleAppListItems += "<li>" + curListOfAppsFromWorkspace[a].name + " (" + curListOfAppsFromWorkspace[a].id + ")</li>"
                            }
                            $("#accessibleApps").html(accessibleAppListItems);
                        }
                        else {
                            $("#accessibleApps").html("<li>Sorry, nothing found...</li>");
                        }
                    }).error(function () {
                        $("#accessibleApps").html("<li>Non-applicable at the moment</li>");
                    });
            } else {
                $("#accessibleApps").html("<li>Non-applicable at the moment</li>");
            }
        }).error(function () {
                $("#accessibleApps").html("<li>Non-applicable at the moment</li>");
            });
        //
    });

    $("#appAccesses").on("hidden", function () {
        $("#accessibleApps").html("");
    });

    $("#username").on("focus", function () {
        $("#selectOrganization").html("");
        $("#selectWorkspace").html("");
        $(".chooseSomething").fadeOut("fast");
    });

    $("#password").on("focus", function () {
        $("#selectOrganization").html("");
        $("#selectWorkspace").html("");
        $(".chooseSomething").fadeOut("fast");
    });

    /*** Uninstaller Trigger ***/
    $("#uninstall").on("click", function () {
        $.post("/podio/installer/uninstall", function (response) {
            if (response.status == "success")
                location.reload();
            else
                alert("There was an unresolvable problem!");
        });
    });
});