class AbstractApplication < Podio::Application

  def self.get_apps_list_by_space(spaceid)
    begin
      Podio::Application.find_all_for_space(spaceid)
    rescue
      []
    end
  end

  def self.test_authentication?(user, pass)
    begin
      Podio.client.authenticate_with_credentials(user, pass)
      return true
    rescue
      return false
    end
  end

  def self.app_exists?(appid)
    begin
      Podio::Application.find(appid)
      return true
    rescue
      return false
    end
  end

end