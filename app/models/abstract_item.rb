class AbstractItem < Podio::Item
  # To change this template use File | Settings | File Templates.

  @@abbrErr = "PAI"

  # Find all items in the app
  def self.range(app_id, external_ids = [{}], attrs = {:order => "ASC", :field => "item_id", :limit => -1, :offset => -1}, filter_by = [{}])
    #begin
    #collection = self.find_by_filter_values(app_id, {}, { :limit => 1, :offset => 1 })
    attrs[:order] = attrs[:order].nil? ? "ASC" : attrs[:order]
    attrs[:field] = attrs[:field].nil? ? "item_id" : attrs[:field]
    attrs[:limit] = (attrs[:limit].nil? || attrs[:limit] == -1) ? 30 : attrs[:limit]
    attrs[:offset] = (attrs[:offset].nil? || attrs[:offset] == -1) ? 0 : attrs[:offset]

    # Check the app if it exists?
    if (AbstractApplication.app_exists?(app_id) == false)
      raise "AbstractItem - #{@@abbrErr}001 The app doesn't exist or service account doesn't have access to it"
    end

    # Start getting data
    collection = nil

    begin
      collection = self.find_by_filter_values(app_id, {}, {:limit => attrs[:limit], :offset => attrs[:offset]})
    rescue
      raise "AbstractItem - #{@@abbrErr}002 The access to the app was restricted"
    end

    data = collection[:all]

    tArray = Array.new
    tAttributes = Array.new
    # Flag for getting all existent attributes
    tAttributesFlag = true

    data.each do |item|
      tHash = Hash.new

      # Populate items into a customized array of items
      tHash['item_id'] = item.attributes[:item_id]
      external_ids.each do |id|
        if (id[:simple])
          value = field_values_by_external_id(item, id[:external_id], {:simple => true})

          if !value.nil?
            tHash[id[:external_id]] = value
            if (tAttributesFlag)
              tAttributes << id[:external_id]
            end
          end
        else
          value = field_values_by_external_id(item, id[:external_id], {})

          if !value.nil?
            tHash[id[:external_id]] = value
            if (tAttributesFlag)
              tAttributes << id[:external_id]
            end
          end
        end
      end

      # Set flag to false to prevent recursively adding fields
      tAttributesFlag = false
      tArray << tHash

    end

    #raise "#{tAttributes.count.to_s}"

    # Change and specify datatypes if needed
    tArray.each do |item|
      tAttributes.each do |attr|
        item[attr] = Float(item[attr]) rescue item[attr].to_s
      end
    end

    # Sort items in the customized array of items
    if (attrs[:order] == "ASC")
      tArray.sort! { |a, b| a[attrs[:field]] <=> b[attrs[:field]] }
    elsif (attrs[:order] == "DESC")
      tArray.sort! { |a, b| a[attrs[:field]] <=> b[attrs[:field]] }
      tArray.reverse!
    end

    tArray
    #rescue Exception => ex
    #raise 'Access denied, please check your podio access permission!'
    #end
  end

  # Find all items based on query
  # def self.all_by_query(app_id, query = {})
  #	if (query.empty?)
  #		raise "AbstractItem - #{@@abbrErr}003 Query must not be empty!"
  #	else
  #		collection = self.find_by_filter_values(app_id, query)
  #		collection[:all]
  #	end
  # end

  # Find all items with options and query
  # def self.all_by_query(app_id, query = {}, options = {})
  #	if (query.empty?)
  #		raise "AbstractItem - #{@@abbrErr}003 Query must not be empty!"
  #	else
  #		collection = self.find_by_filter_values(app_id, query)
  #		collection[:all]
  #	end
  # end

  # Find a range of items with options and query
  # def self.find_range_by_query(app_id, query = {}, options = {})
  #	if (query.empty?)
  #		raise "AbstractItem - #{@@abbrErr}003 Query must not be empty!"
  #	else
  #
  #	end
  # end

  #
  # COMMENT SECTION
  #
  # CONSTANTS
  IS_LIKED = :is_liked
  CREATED_BY = :created_by
  CREATED_BY_TYPE = :type
  CREATED_BY_AVATAR = :avatar
  CREATED_BY_USER_ID = :user_id
  CREATED_BY_IMAGE = :image
  CREATED_BY_LAST_SEEN_ON = :last_seen_on
  CREATED_BY_AVATAR_TYPE = :avatar_type
  CREATED_BY_NAME = :name
  CREATED_BY_ID = :id
  CREATED_BY_AVATAR_ID = :avatar_id
  CREATED_BY_URL = :url
  VALUE = :value
  CREATED_ON = :created_on
  FILES = :files
  COMMENT_ID = :comment_id
  RICH_VALUE = :rich_value
  LIKE_COUNT = :like_count
  #
  #
  #

  # Find all comments in an item
  # def self.comments(item_id)
  #	Podio::Comment.find_all_for('item',item_id)
  # end

  # Get comment's attribute
  # def self.comment_attribute(comment, attribute = :value, options = [])
  #	raw_comment = comment.attributes[attribute]
  #	if (options.empty?)
  #		begin
  #			raw_comment
  #		rescue
  #			raise "AbstractItem - #{@@abbrErr}004 Invalid Operation"
  #		end
  #	else
  #		begin
  #			tHash = Hash.new
  #			options.each do |t|
  #				tHash[t] = raw_comment[t]
  #			end
  #			tHash
  #		rescue
  #			raise "AbstractItem - #{@@abbrErr}004 Invalid Operation"
  #		end
  #	end
  # end

  # Get current database connection name
  # def self.database_conn_name
  #	ActiveRecord::Base.connection.adapter_name.downcase
  # end

  protected

  # Default methods
  def self.field_values_by_external_id(item, external_id, options = {})
    if item.attributes[:fields].present?
      fields = item.attributes[:fields]
      matched = false

      fields.each do |field|
        if (field['external_id'] == external_id)
          values = field['values']
          matched = true
          if (options[:simple])
            return values.first['value']
          else
            return values
          end
        end
      end

      if !matched
        nil
      end
    else
      nil
    end
  end

end