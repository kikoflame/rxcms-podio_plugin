Rails.application.routes.draw do
    
    # Mandatory routes
    get "/podio/engine/readme" => "engine#index"
    get "/podio/engine/installer" => "engine#installer"
    get "/podio/engine/configure" => "engine#configure"
    
    # Mandatory installer routes
    post "/podio/installer/before_process" => "installer#before_process"
    post "/podio/installer/core_process" => "installer#core_process"
    post "/podio/installer/post_process" => "installer#post_process"
    post "/podio/installer/uninstall" => "installer#uninstall"
    
    # Optional routes
    get "engine/services"
    
    # Authentication routes
    # match '/podio/auth/podio' => 'sessions#create_session'
    # match '/podio/auth/podio/callback' => 'sessions#success_callback'
    match '/podio/auth/podio/delete' => 'sessions#delete_session'
    match '/podio/auth/podio_as_user' => 'sessions#create_session_as_user'
    match '/podio/auth/podio_change_user' => 'sessions#change_session_as_user'
    
    # Utility routes
    get '/ext/podio/data/:appid/:fields/:order/:limit/:offset' => 'podio#get_podio_items', :constraints => { :fields => /([a-zA-Z]+\-{0,1}[a-zA-Z]+(%7){0,1})+/ }
    get '/ext/podio/data/:appid/:fields/:order' => 'podio#get_podio_items', :constraints => { :fields => /([a-zA-Z]+\-{0,1}[a-zA-Z]+(%7){0,1})+/ }
    get '/ext/podio/data/:appid/:fields/:order/:limit' => 'podio#get_podio_items', :constraints => { :fields => /([a-zA-Z]+\-{0,1}[a-zA-Z]+(%7){0,1})+/ }
    get '/ext/podio/data/:appid/:fields/:order/:offset' => 'podio#get_podio_items', :constraints => { :fields => /([a-zA-Z]+\-{0,1}[a-zA-Z]+(%7){0,1})+/ }
    get '/ext/podio/data/:appid/:fields' => 'podio#get_podio_items', :constraints => { :fields => /([a-zA-Z]+\-{0,1}[a-zA-Z]+(%7){0,1})+/ }
    
    # Servicing routes
    get '/podio/app/active/:appid' => 'podio#get_podio_app_status'
    post '/podio/user/login' => 'podio#check_user_login'
    get '/podio/data/orgs' => 'podio#get_list_of_organizations_of_user'
    get '/podio/orgs/space/:space' => 'podio#get_list_of_apps_from_workspace'
    post '/podio/user/manipulate' => 'services#update_or_create_podio_service_account'
    get '/podio/orgs/curspace' => 'services#get_current_podio_workspace'
    post '/podio/orgs/space/manipulate/' => 'services#set_current_podio_workspace'
    get '/podio/orgs/curspace/apps' => 'podio#get_list_of_apps_for_elements'
    get '/podio/user/current' => 'services#get_current_podio_user'
    
end
